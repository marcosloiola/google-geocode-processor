/**
 * 
 */
package com.mlit.geocodeprocessor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import com.google.gson.Gson;
import com.mlit.geocodeprocessor.bean.Address_components;
import com.mlit.geocodeprocessor.bean.GeocodeResult;
import com.mlit.geocodeprocessor.bean.Geometry;
import com.mlit.geocodeprocessor.bean.Location;
import com.mlit.geocodeprocessor.bean.Results;
import com.mlit.geocodeprocessor.utils.HttpUtils;

/**
 * @author marcosloiola1
 * 
 */
public class ProcessGeocode {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {

			File sourceFile = new File("input.txt");

			File destinyFile = new File("output.txt");

			File tempFile = new File("temp.txt");

			File errorFile = new File("error.txt");

			FileReader fr = new FileReader(sourceFile);

			FileWriter fw = new FileWriter(destinyFile, true);

			FileWriter fwTemp = new FileWriter(tempFile);

			FileWriter fwError = new FileWriter(errorFile, true);

			BufferedReader br = new BufferedReader(fr);

			String line = "";

			int i = 0;

			while ((line = br.readLine()) != null) {

				try {

					String[] params = line.split("\\|");

					if (i < 2500) {

						GeocodeResult geocode = getLocation(params[1]);
						Results result = geocode.getResults().get(0);

						String neighborhood = "";
						String zipCode = "";

						for (Address_components address_components : result.getAddress_components()) {
							for (String type : address_components.getTypes()) {
								if ("neighborhood".equals(type)) {
									neighborhood = address_components.getLong_name();
								} else if ("postal_code".equals(type)) {
									zipCode = address_components.getLong_name();
								}
							}
						}

						Geometry geometry = result.getGeometry();
						Location location = geometry.getLocation();
						Number lat = location.getLat();
						Number lng = location.getLng();

						fw.write(line);
						fw.write("|");
						fw.write(neighborhood);
						fw.write("|");
						fw.write(zipCode);
						fw.write("|");
						fw.write(String.valueOf(lat));
						fw.write("|");
						fw.write(String.valueOf(lng));
						fw.write("\n");
						fw.flush();

					} else {

						fwTemp.write(line);
						fwTemp.write("\n");

					}

				} catch (Exception e) {
					fwError.write(line);
					fwError.write("\n");
					fwError.flush();
				}

				i++;

			}

			sourceFile.delete();
			tempFile.renameTo(new File("input.txt"));

			fr.close();
			fw.flush();
			fw.close();
			fwTemp.flush();
			fwTemp.close();
			fwError.flush();
			fwError.close();

			System.out.println("done");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static GeocodeResult getLocation(String requestURL) {

		String json = HttpUtils.request(requestURL);

		return new Gson().fromJson(json, GeocodeResult.class);

	}

}
