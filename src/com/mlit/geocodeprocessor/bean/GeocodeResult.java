package com.mlit.geocodeprocessor.bean;

import java.util.ArrayList;
import java.util.List;

public class GeocodeResult {
	private List<Results> results;
	private String status;

	private GeocodeResult() {
		results = new ArrayList<Results>();
	}

	public List<Results> getResults() {
		return this.results;
	}

	public void setResults(List<Results> results) {
		this.results = results;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
